/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robotproject;

import java.util.Scanner;

/**
 *
 * @author a
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner n = new Scanner(System.in);
        TableMap map = new TableMap(10,10);
        Robot robot = new Robot(2,2,'x', map);
        Bomb bomb = new Bomb(5,5);
        map.setBomb(bomb);
        map.setRobot(robot);
        while(true){
           map.showMap(); 
            char direction = inputDirection(n);
            if(direction=='q'){
                printByeBye();
                break;
            }
           robot.walk(direction);
           
        }
    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner n) {
        String str = n.next();
        char direction = str.charAt(0);
        return direction;
    }
}
