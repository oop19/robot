/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robotproject;

/**
 *
 * @author a
 */
public class Robot {

    private int x;
    private int y;
    private char symbol;
    private TableMap Map;

    public Robot(int x, int y, char symbol, TableMap Map) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
        this.Map = Map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public boolean walk(char direction) {
        switch(direction){
            case 'N':
            case 'w':
                if(walkN())return false;
                break;
            case 'S':
            case 's':
                if(walkS())return false;
                break;
            case 'E':
            case 'd':
                if(walkE())return false;
                break;
            case 'W':
            case 'a':
                if(walkW())return false;
                break;
            default:
                return false;
        }
        checkBomb();
        return true;
    }

    private void checkBomb() {
        if(Map.isBomb(x, y)){
            System.out.println("Found Bomb!!! (" + x + ", " + y +")");
        }
    }

    private boolean walkW() {
        if (Map.inMap(x-1,y)) {
            x = x - 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkE() {
        if (Map.inMap(x+1,y)) {
            x = x + 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkS() {
        if (Map.inMap(x,y+1)) {
            y = y + 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkN() {
        if (Map.inMap(x,y-1)) {
            y = y - 1;
        } else {
            return true;
        }
        return false;
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }
}
